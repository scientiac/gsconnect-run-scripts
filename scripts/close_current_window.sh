#!/bin/bash

device_name=$(~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -l | sed -n '1p') 

xdotool key "alt+F4" 

~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -d $device_name --notification "Terminator" --notification-appname "Connect" --notification-body "Current process has been terminated."
