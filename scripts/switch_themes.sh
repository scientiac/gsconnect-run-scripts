#!/bin/bash

device_name=$(~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -l | sed -n '1p') 

xdotool key "super+d" 

~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -d $device_name --notification "Transformer" --notification-appname "Connect" --notification-body "Switched theme!"
