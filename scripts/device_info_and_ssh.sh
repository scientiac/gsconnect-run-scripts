#!/bin/bash

device_name=$(~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -l | sed -n '1p') 

battery=$(upower -i /org/freedesktop/UPower/devices/DisplayDevice | sed -n '/percentage/p' | tr -d ' ' | sed -n 's/:/: /gp' | sed -n 's/p/P/gp')

total_mem_kb=$(grep MemTotal /proc/meminfo | awk '{print $2}')
total_mem_gb=$(bc <<<"scale=2; $total_mem_kb / 1048576")

avai_mem_kb=$(grep MemAvailable /proc/meminfo | awk '{print $2}')
avai_mem_gb=$(bc <<<"scale=2; $avai_mem_kb / 1048576")

ip_addr=$(ip address | sed -rn '/((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])/p' | sed -n '2p' | tr -d a-z | cut -d " " -f6)
main_ip=$(ip address | sed -rn '/((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])/p' | sed -n '2p' | tr -d a-z | cut -d " " -f6 | sed 's/\(.*\).../\1/')

ssid=$(nmcli -p | sed -n '1p' | cut -d " " -f4)



bat=$(echo "Battery $battery")
mem=$(echo "Available Memory: $avai_mem_gb(GB)/$total_mem_gb(GB)")
ip=$(echo "Device IP: $ip_addr")
ssh=$(echo "ssh $(whoami)@$main_ip")
wifi=$(echo "Connected To: $ssid")


~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -d $device_name --notification "Informer" --notification-appname "Connect " --notification-body "[$bat] [$mem] [$ip] [$wifi]"

~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -d $device_name --share-text "$ssh" 
