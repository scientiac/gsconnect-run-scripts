#!/bin/bash
filepath=/tmp/"$(date --iso)"-"$(date +%s)".png
device_name=$(~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -l | sed -n '1p') 

mplayer -vo png -frames 1 tv://

sleep 1

mv 00000001.png $filepath

~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -d $device_name --share-file $filepath
