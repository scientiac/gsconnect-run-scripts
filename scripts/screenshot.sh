#!/bin/bash
filepath=/tmp/"$(date --iso)"-"$(date +%s)".png
device_name=$(~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -l | sed -n '1p') 

gnome-screenshot --file=$filepath

sleep 1

 ~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -d $device_name --share-file $filepath

