#!/bin/bash
filepath=/tmp/"$(date --iso)"-"$(date +%s)".mp3
device_name=$(~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -l | sed -n '1p') 

rec -c 2 radio.mp3 trim 0 00:00:10

sleep 1

mv radio.mp3 $filepath

~/.local/share/gnome-shell/extensions/gsconnect@andyholmes.github.io/service/daemon.js -d $device_name --share-file $filepath
