# gsconnect-run-scripts
These scripts are activated remotely by the device that's connected to GSconnect to accomplish a particular task.

## Dependencies

> sed

***

#### camclip.sh
Captures a picture from your webcam and sends it to your phone.
> mplayer

***

#### close_current_window.sh
Closes the current running window.
> xdotool

***

#### screenshot.sh
Captures a screenshot of the screen and sends it to your phone.
> gnome-screenshot

***

#### switch_themes.sh
Switches between light and dark themes.
> xdotool

> Night Theme Switcher (Gnome Extension)

***

#### device_info_and_ssh.sh
Sends a notification containing device info and also pushes a command to connect to the device using ssh to phone's clipboard.

> network-manager

> openssh


**Notes:** 
* You don't need ```openssh``` just to get device info but will need if you wish to access your computer via ssh.
* *On your Computer:* To use ssh you need to have ```openssh-server``` installed and ```sshd``` service enabled by running ```sudo systemctl start sshd``` and ```sudo systemctl enable sshd```.
* *On your Phone:* To ssh into your computer, install ```Termux``` then install the ```openssh``` package inside termux. After setting up, access your computer by pasting the command pushed to your clipboard [e.g. ssh user@192.168.1.78] in termux and following along.

***

#### audioclip.sh
Captures audio for 10 seconds and sends it to your phone.
> sox

***

## Instructions

* Clone this repository to your desired directory.
* ```cd``` into the ```scripts``` directory and run ```chmod +x *```
* Install the required dependencies ```listed above```.
* Goto ```Night Theme Switcher settings>advanced>keyboard shortcut>set "super + d"``` 
* Goto ```GSconnect settings> your device> commands> add (+)> add a name``` [e.g. Screenshot]
* Put the path to a particular script [e.g. ```/home/user/Experiments/gsconnect-run-scripts/scripts/screenshot.sh```] 
* Repeat the process and configure everything.

***

### Other Simple Commands

1. Poweroff: ```systemctl ppoweroff```
2. Reboot: ```systemctl reboot```
3. Lock Screen: ```loginctl lock-session```
4. Unlock Screen: ```loginctl unlock-session```
5. Suspend: ```systemctl suspend```
6. Hybernate: ```systemctl hybernate```
